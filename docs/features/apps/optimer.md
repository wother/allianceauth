# Fleet Operations

Fleet Operations is an app for organizing and communicating fleet schedules.

![optimer](/_static/images/features/apps/optimer.png)

## Installation

Add `'allianceauth.optimer',` to your `INSTALLED_APPS` list in your auth project's settings file. Run migrations to complete installation.
